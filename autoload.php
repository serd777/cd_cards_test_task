<?php

/**
 * Загрузка файлов ядра
 */
function __loadCore() {
	$routes = [];

	foreach (glob(ROOTDIR . "/core/*.php") as $filename) {
		include $filename;
	}
	foreach (glob(ROOTDIR . "/core/services/*.php") as $filename) {
		include $filename;
	}

	//  Загрузка файлов проекта
	foreach (glob(ROOTDIR . "/models/*.php") as $filename) {
		include $filename;
	}
	foreach (glob(ROOTDIR . "/controllers/*.php") as $filename) {
		include $filename;
	}
	foreach (glob(ROOTDIR . "/routes/*.php") as $filename) {
		$arr = require $filename;

		foreach($arr as $item) {
			$routes[] = $item;
		}
	}

	$GLOBALS['routes'] = $routes;
}
__loadCore();
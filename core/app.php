<?php namespace App;

use App\Core\Services\Db;

/**
 * Основной класс ядра фреймворка
 * @package App
 */
class App {
	/**
	 * Класс доступа к СУБД
	 * @var Db
	 */
	private $db;

	/**
	 * Конструктор
	 */
	public function __construct() {
		$this->db = new Db();
	}

	/**
	 * Метод доступа к СУБД
	 * @return Db
	 */
	public function getDb() {
		return $this->db;
	}

	/**
	 * Метод инициализации
	 */
	public function init() {
		$this->db->init($this->getConfig('db'));
	}

	/**
	 * Метод, инициализирующй нужный роутинг
	 */
	public function initRoute() {
		$request_uri = $_SERVER['REQUEST_URI'];
		$routes = $GLOBALS['routes'];

		foreach ($routes as $route) {
			if($route['path'] === $request_uri) {
				$name = '\\App\\Controllers\\' . $route['controller'];
				$method = $route['method'];

				$controller = new $name();
				$controller->$method();
				break;
			}
		}
	}

	/**
	 * Метод, подключающий CSS по указанному пути
	 * @param $file_path
	 */
	public function includeCss($file_path) {
		echo '<style>';
			echo file_get_contents(ROOTDIR . $file_path);
		echo '</style>';
	}

	/**
	 * Метод, подключающий JS по указанному пути
	 * @param $file_path
	 */
	public function includeJs($file_path) {
		echo '<script type="text/javascript">';
			echo file_get_contents(ROOTDIR . $file_path);
		echo '</script>';
	}

	/**
	 * Метод, подключающий вложенный view
	 * @param $file_path
	 */
	public function includeSubView($file_path) {
		require ROOTDIR . '/views/' . $file_path . '.php';
	}

	/**
	 * Метод, загружающий визуальное отображение
	 * @param $view_name
	 */
	public function loadView($view_name) {
		require ROOTDIR . '/views/' . $view_name . '.php';
	}

	/**
	 * Метод, возвращающий данные из файла в конфиге
	 * @param $config_name
	 *
	 * @return array
	 */
	public function getConfig($config_name) {
		return require ROOTDIR . '/config/' . $config_name . '.php';
	}
}
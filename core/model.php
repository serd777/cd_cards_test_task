<?php namespace App\Core;

/**
 * Класс прототипа моделей
 * @package App\Core
 */
class Model {
	/**
	 * Имя таблицы
	 * @var string
	 */
	private $table_name;

	/**
	 * Массив полей в БД
	 * @var array
	 */
	protected $fields;

	/**
	 * Конструктор класса
	 *
	 * @param string $table_name
	 */
	public function __construct($table_name) {
		$this->table_name = $table_name;
		$this->fields = ['id' => null];
	}

	/**
	 * Метод, возвращающий все записи
	 * @return array
	 */
	public function all() {
		return app()->getDb()->querySelect("SELECT * FROM $this->table_name");
	}

	/**
	 * Метод, обновляющий модель в БД
	 */
	public function update() {
		$query = "UPDATE $this->table_name SET ";
		$args_length = count($this->fields);
		$args = []; $index = 1;

		foreach($this->fields as $key => $value) {
			if($key != 'id') {
				$query .= $key . ' = ?';
				$args[] = $value;
				$index++;

				if($index != $args_length)
					$query .= ', ';
			}
		}
		$query .= ' WHERE id = ?';
		$args[] = $this->fields['id'];

		app()->getDb()->queryUpdate($query, $args);
	}

	/**
	 * Метод, сохраняющий модель в БД
	 */
	public function create() {
		$query = "INSERT INTO $this->table_name (";
		$args_length = count($this->fields);

		$index = 0;
		foreach($this->fields as $key => $value) {
			$query .= $key;

			$index++;
			if($index != $args_length)
				$query .= ', ';
		}

		$query .= ') VALUES(';
		$index = 0; $args = [];
		foreach($this->fields as $key => $value) {
			$query .= '?';
			$args[] = $value;
			$index++;

			if($index != $args_length)
				$query .= ', ';
			else $query .= ')';
		}

		app()->getDb()->queryUpdate($query, $args);
		$this->fields['id'] = app()->getDb()->querySelect('SELECT LAST_INSERT_ID() AS id')[0]['id'];
	}

	/**
	 * Метод, удаляющий указанную модель из БД
	 */
	public function delete() {
		if($this->fields['id']) {
			$id = $this->fields['id'];
			app()->getDb()->queryUpdate("DELETE FROM $this->table_name WHERE id = ?", [$id]);
		}
	}
}
<?php namespace App\Core;

/**
 * Класс реализующий модель контроллера
 * @package App\Core
 */
class Controller {
	/**
	 * Возвращает переданный параметр
	 * @param $name
	 *
	 * @return mixed
	 */
	public function getInput($name) {
		$data = json_decode(file_get_contents('php://input'), true);
		return $data[$name];
	}

	/**
	 * Сохраняет входной PNG файл
	 *
	 * @param string $img
	 * @param string $name
	 *
	 * @return string
	 */
	public function saveInputFile($img, $name) {
		if (!file_exists(ROOTDIR . '/public/img/'))
			mkdir(ROOTDIR . '/public/img/');

		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$img = base64_decode($img);

		$path = '/public/img/' . md5($name . time()) . '.png';
		file_put_contents(ROOTDIR . $path, $img);

		return $path;
	}
}
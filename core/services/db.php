<?php namespace App\Core\Services;

/**
 * Сервис взаимодействия с БД
 * @package App\Core\Services
 */
class Db {
	/**
	 * Доступ к mysql
	 * @var \mysqli
	 */
	private $mysql;

	/**
	 * Конструктор
	 */
	public function __construct() {}

	/**
	 * Метод, инициализирующий подключение к СУБД
	 * @param $creds - данные аутентификации
	 */
	public function init($creds) {
		//  Инициализируем подключение
		$this->mysql = new \mysqli($creds['host'], $creds['login'], $creds['password'], $creds['db_name']);

		if ($this->mysql->connect_errno) {
			echo "Ошибка: Не удалась создать соединение с базой MySQL и вот почему: \n";
			echo "Номер ошибки: " . $this->mysql->connect_errno . "\n";
			echo "Ошибка: " . $this->mysql->connect_error . "\n";

			exit;
		}
	}

	/**
	 * Метод, делающий выборку из БД
	 * @param string $query
	 * @param array $params
	 *
	 * @return array
	 */
	public function querySelect($query, $params = []) {
		$result = [];
		$statement = $this->executeStatement($query, $params);

		$select_result = $statement->get_result();
		while ($row = $select_result->fetch_assoc())
			$result[] = $row;

		return $result;
	}

	/**
	 * Метод, делающий обновление записи
	 * @param $query
	 * @param $params
	 */
	public function queryUpdate($query, $params) {
		$this->executeStatement($query, $params);
	}

	/**
	 * Метод, выполняющий запрос к СУБД
	 * @param $query
	 * @param $params
	 *
	 * @return \mysqli_stmt
	 */
	private function executeStatement($query, $params) {
		$statement = $this->mysql->prepare($query);

		if($statement === false) {
			echo "Ошибка: Не удалась сформировать запрос: \n";
			echo "Запрос: " . $query . "\n";

			exit;
		}

		$param_str = '';
		foreach($params as $param) {
			if(is_double($param)) {
				$param_str .= 'd';
			} else if(is_int($param)) {
				$param_str .= 'i';
			} else {
				$param_str .= 's';
			}
		}

		//  Если есть параметры, то нужно линковать
		if($param_str != '') {
			array_unshift($params, $param_str);

			$tmp_arr = [];
			foreach($params as $key => $value)
				$tmp_arr[$key] = &$params[$key];

			call_user_func_array(array(&$statement, 'bind_param'), $tmp_arr);
		}

		$res_code = $statement->execute();
		if(!$res_code) {
			echo "Ошибка: Не удалась выполнить запрос: \n";
			echo "Ошибка: " . $statement->error . "\n";

			exit;
		}

		return $statement;
	}
}
//
//  Работа с карточками
//

function Cards() {
    this.cards = [];
    this.selected_card = {};

    this.sort_amount = false;

    this.getCards = function() {
        globalRequests.get(
            '/cards/list',
            function(data) {
                globalCards.cards = data;
                globalCards.updateCards();
            },
            function(err) {
                console.error(err);
            }
        );
    };

    this.addCard = function(card) {
        globalRequests.post(
            '/cards/add',
            card,
            function(data) {
                globalCards.cards.push(data);
                globalCards.updateCards();

                $('#modalAddDisk').modal('toggle');
            },
            function(err) {
                console.error(err);
            }
        )
    };

    this.deleteCard = function(index) {
        globalRequests.post(
            '/cards/delete',
            {id: this.cards[index].id}
        );
        this.cards.splice(index, 1);
        this.updateCards();
    };

    this.editCard = function() {
        globalRequests.post(
            '/cards/update',
            this.selected_card,
            function(data) {
                const item = globalCards.cards.filter(function(elem) { return elem.id === data.id; })[0];
                if(item) {
                    const index = globalCards.cards.indexOf(item);
                    globalCards.cards[index] = data;
                    globalCards.updateCards();

                    $('#modalEditDisk').modal('toggle');
                }
            },
            function(err) {
                console.error(err);
            }
        )
    };
    this.selectCard = function(index) {
        this.selected_card = Object.assign({}, this.cards[index]);
        modals.showEditDisk(this.selected_card);
    };

    this.updateCards = function() {
        var table = $('#cardsTable tbody');
        table.html('');

        for(var i = 0; i < this.cards.length; i++) {
            table.append(
                    "<tr>" +
                        "<td class='text-center'><img src='" + this.cards[i].paper + "' width='100px' height='100px'/></td>" +
                        "<td class='text-center'>" + this.cards[i].name + "</td>" +
                        "<td class='text-center'>" + this.cards[i].author + "</td>" +
                        "<td class='text-center'>" + this.cards[i].release_year + "</td>" +
                        "<td class='text-center'>" + this.cards[i].duration + "</td>" +
                        "<td class='text-center'>" + this.cards[i].buyed_at + "</td>" +
                        "<td class='text-center'>" + this.cards[i].amount + "</td>" +
                        "<td class='text-center'>" + this.cards[i].code_placement + "</td>" +
                        "<td class='text-center'>" +
                            "<button class='btn btn-info' onclick='globalCards.selectCard(" + i + ")'>Изменить</button>" +
                            "<button class='btn btn-danger' onclick='globalCards.deleteCard(" + i +")'>Удалить</button>" +
                        "</td>" +
                    "</tr>"
            );
        }

        table.append(
            "<tr>" +
                "<td class='text-right' colspan='9'>" +
                    "<button class='btn btn-success' onclick='modals.showAddDisk()'>+</button>" +
                "</td>" +
            "</tr>"
        );
    };

    this.sortViaPrice = function() {
        this.cards.sort(function(a, b) { return a.amount - b.amount; });

        this.sort_amount = !this.sort_amount;
        if(this.sort_amount) {
            this.cards.reverse();
        }

        this.updateCards();
    }
}
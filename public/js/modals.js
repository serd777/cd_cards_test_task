//
//  Модальные окна
//

function Modals() {
    //  Модель нового диска
    this.new_disk = {};

    //  Открытие окна добавления
    this.showAddDisk = function() {
        this.resetNewDisk();
        $('#modalAddDisk').modal();
    };
    //  Открытие окна редактировния
    this.showEditDisk = function(card) {
        $('#modalEditDisk').modal();
        this.setEditFields(card);
    };

    this.resetNewDisk = function() {
        this.new_disk = {
            'paper' : '',
            'name'  : '',
            'author' : '',
            'release_year' : 2019,
            'duration' : 1,
            'buyed_at' : null,
            'amount' : 0,
            'code_placement' : ''
        };
    };
    this.setEditFields = function(card) {
        $('#editDiskName').val(card.name);
        $('#editDiskAuthor').val(card.author);
        $('#editDiskYear').val(card.release_year);
        $('#editDiskDuration').val(card.duration);
        $('#editDiskBuyedAt').val(card.buyed_at);
        $('#editDiskPrice').val(card.amount);
        $('#editDiskCode').val(card.code_placement);
    };
}
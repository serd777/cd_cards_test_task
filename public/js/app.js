var modals = new Modals();
var globalRequests = new Requests();
var globalCards = new Cards();

//
//  Скрипт выполняющийся при запуске
//

$(function() {
    globalCards.getCards();

    //  Привяжем данные к изменению данных окна добавления
    $('#newDiskPaper').change(function() {
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            modals.new_disk.paper = reader.result;
        }, false);

        var file  = (this).files[0];
        if (file) {
            reader.readAsDataURL(file);
        }
    });
    $('#newDiskName').change(function() {modals.new_disk.name = (this).value;});
    $('#newDiskAuthor').change(function() {modals.new_disk.author = (this).value;});
    $('#newDiskYear').change(function() {modals.new_disk.release_year = (this).value;});
    $('#newDiskDuration').change(function() {modals.new_disk.duration = (this).value;});
    $('#newDiskBuyedAt').change(function() {modals.new_disk.buyed_at = (this).value;});
    $('#newDiskPrice').change(function() {modals.new_disk.amount = (this).value;});
    $('#newDiskCode').change(function() {modals.new_disk.code_placement = (this).value;});

    //  Привяжем данные к изменению данных окна изменения
    $('#editDiskPaper').change(function() {
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            globalCards.selected_card.paper = reader.result;
        }, false);

        var file  = (this).files[0];
        if (file) {
            reader.readAsDataURL(file);
        }
    });
    $('#editDiskName').change(function() {globalCards.selected_card.name = (this).value;});
    $('#editDiskAuthor').change(function() {globalCards.selected_card.author = (this).value;});
    $('#editDiskYear').change(function() {globalCards.selected_card.release_year = (this).value;});
    $('#editDiskDuration').change(function() {globalCards.selected_card.duration = (this).value;});
    $('#editDiskBuyedAt').change(function() {globalCards.selected_card.buyed_at = (this).value;});
    $('#editDiskPrice').change(function() {globalCards.selected_card.amount = (this).value;});
    $('#editDiskCode').change(function() {globalCards.selected_card.code_placement = (this).value;});
});
//
//  Класс взаимодействия с запросами
//

function Requests() {
    this.get = function(url, callbackSuccess, callbackError) {
        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: callbackSuccess,
            failure: callbackError
        });
    };
    this.post = function(url, data, callbackSuccess, callbackError) {
        $.ajax({
            type: 'POST',
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: callbackSuccess,
            failure: callbackError
        });
    };
}
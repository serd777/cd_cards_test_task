<?php

//  Глобальные переменные

define('ROOTDIR', __DIR__);
require ROOTDIR . '/autoload.php';

/**
 * Возвращает основное приложение
 * @return \App\App
 */
function app() {
	return $GLOBALS['app'];
}

//  Инициализация ядра
$GLOBALS['app'] = new App\App();
$GLOBALS['app']->init();
$GLOBALS['app']->initRoute();
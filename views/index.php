<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>CD Cards</title>

        <?php app()->includeCss('/public/vendors/bootstrap/bootstrap.min.css') ?>
		<?php app()->includeCss('/public/css/app.css') ?>
	</head>
	<body>
        <div class="container">
            <div class="row">
                <h3 class="text-center">Редактор CD обложек</h3>
                <hr />

                <button class="btn btn-info" onclick="globalCards.sortViaPrice()">Сортировать по стоимости</button>

                <table class="table table-responsive" id="cardsTable">
                    <thead>
                        <tr>
                            <th>Обложка</th>
                            <th>Название альбома</th>
                            <th>Название артиста</th>
                            <th>Год выпуска</th>
                            <th>Длительность альбома</th>
                            <th>Дата покупки</th>
                            <th>Стоимость покупки</th>
                            <th>Код хранилища</th>
                            <th class="col-lg-3"></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

        <!-- Модальные окна -->
        <?php app()->includeSubView('index/add_disk.modal') ?>
        <?php app()->includeSubView('index/edit_disk.modal') ?>

	    <?php app()->includeJs('/public/vendors/jquery/jquery-3.3.1.min.js') ?>
        <?php app()->includeJs('/public/vendors/bootstrap/bootstrap.min.js') ?>

        <!-- JS файлы приложения -->
        <?php app()->includeJs('/public/js/modals.js') ?>
        <?php app()->includeJs('/public/js/requests.js') ?>
        <?php app()->includeJs('/public/js/cards.js') ?>
        <?php app()->includeJs('/public/js/app.js') ?>
	</body>
</html>
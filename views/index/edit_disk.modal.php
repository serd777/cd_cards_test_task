<div class="modal fade" tabindex="-1" role="dialog" id="modalEditDisk">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Редактирование карточки</h4>
			</div>
			<div class="modal-body">
				<table class="table table-responsive">
					<tr>
						<td class="text-center">Обложка</td>
						<td class="text-center">
							<input type="file" id="editDiskPaper" accept="image/x-png" />
						</td>
					</tr>
					<tr>
						<td class="text-center">Название альбома</td>
						<td class="text-center">
							<input type="text" class="form-control" id="editDiskName"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Название артиста</td>
						<td class="text-center">
							<input type="text" class="form-control" id="editDiskAuthor"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Год выпуска</td>
						<td class="text-center">
							<input type="number" class="form-control" id="editDiskYear"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Длительность альбома</td>
						<td class="text-center">
							<input type="number" class="form-control" id="editDiskDuration"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Дата покупки</td>
						<td class="text-center">
							<input type="date" class="form-control" id="editDiskBuyedAt"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Стоимость покупки</td>
						<td class="text-center">
							<input type="number" class="form-control" id="editDiskPrice"/>
						</td>
					</tr>
					<tr>
						<td class="text-center">Код хранилища</td>
						<td class="text-center">
							<input type="text" class="form-control" id="editDiskCode"/>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<button type="button" class="btn btn-primary" onclick="globalCards.editCard()">Сохранить</button>
			</div>
		</div>
	</div>
</div>
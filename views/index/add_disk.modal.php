<div class="modal fade" tabindex="-1" role="dialog" id="modalAddDisk">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Добавление новой карточки</h4>
			</div>
			<div class="modal-body">
				<table class="table table-responsive">
                    <tr>
                        <td class="text-center">Обложка</td>
                        <td class="text-center">
                            <input type="file" id="newDiskPaper" accept="image/x-png" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Название альбома</td>
                        <td class="text-center">
                            <input type="text" class="form-control" id="newDiskName"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Название артиста</td>
                        <td class="text-center">
                            <input type="text" class="form-control" id="newDiskAuthor"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Год выпуска</td>
                        <td class="text-center">
                            <input type="number" class="form-control" id="newDiskYear"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Длительность альбома</td>
                        <td class="text-center">
                            <input type="number" class="form-control" id="newDiskDuration"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Дата покупки</td>
                        <td class="text-center">
                            <input type="date" class="form-control" id="newDiskBuyedAt"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Стоимость покупки</td>
                        <td class="text-center">
                            <input type="number" class="form-control" id="newDiskPrice"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Код хранилища</td>
                        <td class="text-center">
                            <input type="text" class="form-control" id="newDiskCode"/>
                        </td>
                    </tr>
                </table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<button type="button" class="btn btn-primary" onclick="globalCards.addCard(modals.new_disk)">Добавить</button>
			</div>
		</div>
	</div>
</div>
<?php namespace App\Models;

use App\Core\Model;

/**
 * Модель CD карточек
 * @package App\Models
 */
class Card extends Model {
	/**
	 * Конструктор
	 */
	public function __construct() {
		parent::__construct('cards');

		$this->fields['paper'] = '';
		$this->fields['name'] = '';
		$this->fields['author'] = '';
		$this->fields['release_year'] = '';
		$this->fields['duration'] = '';
		$this->fields['buyed_at'] = '';
		$this->fields['amount'] = '';
		$this->fields['code_placement'] = '';
	}

	/**
	 * Доступ к полям
	 * @return array
	 */
	public function &getFields() {
		return $this->fields;
	}
}
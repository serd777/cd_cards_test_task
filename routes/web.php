<?php return [
	//  Главная страница
	['path' => '/', 'controller' => 'navigationController', 'method' => 'index'],

	//  Список карточек
	['path' => '/cards/list', 'controller' => 'cardsController', 'method' => 'list'],
	['path' => '/cards/add', 'controller' => 'cardsController', 'method' => 'add'],
	['path' => '/cards/update', 'controller' => 'cardsController', 'method' => 'update'],
	['path' => '/cards/delete', 'controller' => 'cardsController', 'method' => 'delete']
];
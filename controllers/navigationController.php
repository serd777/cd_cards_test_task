<?php namespace App\Controllers;

use App\Core\Controller;

/**
 * Контроллер обработки запроса страницы
 * @package App\Controllers
 */
class NavigationController extends Controller {
	/**
	 * Главная страница сайта
	 */
	public function index() {
		app()->loadView('index');
	}
}
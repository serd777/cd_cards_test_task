<?php namespace App\Controllers;

use App\Core\Controller;
use App\Models\Card;

/**
 * Контроллер взаимодействия с карточками
 * @package App\Controllers
 */
class CardsController extends Controller {
	/**
	 * Список карточек
	 */
	public function list() {
		$cards = new Card();
		echo json_encode($cards->all());
	}

	/**
	 * Добавление новой карточки
	 */
	public function add() {
		$card = new Card();

		$card->getFields()['paper'] = $this->saveInputFile($this->getInput('paper'), $this->getInput('name'));
		$card->getFields()['name'] = $this->getInput('name');
		$card->getFields()['author'] = $this->getInput('author');
		$card->getFields()['release_year'] = $this->getInput('release_year');
		$card->getFields()['duration'] = $this->getInput('duration');
		$card->getFields()['buyed_at'] = $this->getInput('buyed_at');
		$card->getFields()['amount'] = $this->getInput('amount');
		$card->getFields()['code_placement'] = $this->getInput('code_placement');

		$card->create();
		echo json_encode($card->getFields());
	}

	/**
	 * Изменение карточки
	 */
	public function update() {
		$card = new Card();

		if($this->getInput('paper'))
			$card->getFields()['paper'] = $this->saveInputFile($this->getInput('paper'), $this->getInput('name'));

		$card->getFields()['id'] = $this->getInput('id');
		$card->getFields()['name'] = $this->getInput('name');
		$card->getFields()['author'] = $this->getInput('author');
		$card->getFields()['release_year'] = $this->getInput('release_year');
		$card->getFields()['duration'] = $this->getInput('duration');
		$card->getFields()['buyed_at'] = $this->getInput('buyed_at');
		$card->getFields()['amount'] = $this->getInput('amount');
		$card->getFields()['code_placement'] = $this->getInput('code_placement');

		$card->update();
		echo json_encode($card->getFields());
	}

	/**
	 * Удаление карточки
	 */
	public function delete() {
		$card = new Card();
		$card->getFields()['id'] = $this->getInput('id');

		$card->delete();
		echo 'OK';
	}
}